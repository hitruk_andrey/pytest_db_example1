import pytest
import mysql.connector
import yaml


@pytest.fixture
def connect_db_one():
    config = yaml.safe_load(open('config.yml'))
    cnx = mysql.connector.connect(**config)
    cnx.close()