import pytest
import mysql.connector


@pytest.fixture
def connect_db_one():
    cnx = mysql.connector.connect(host='host', user='user',
                                  password='password',
                                  database='database')
    cursor = cnx.cursor()
    yield cursor
    cnx.close()