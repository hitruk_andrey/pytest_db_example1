import pytest
import mysql.connector


@pytest.fixture
def connect_db_three():
    cnx = mysql.connector.connect(host='host', user='user',
                                  password='password',
                                  database='database')
    cursor = cnx.cursor()
    try:
        yield cursor
    except:
        cnx.clouse()