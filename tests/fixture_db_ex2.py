# Блоку except можно добавить необязательныйблок else,
# который сработает в случае, если программа выполнилась без ошибок:
#
# try:
#     a = float(input("Введите число: ")
#     print(100 / a)
# except ValueError:
#     print("Это не число!")
# except ZeroDivisionError:
#     print("На ноль делить нельзя!")
# except:
#     print("Неожиданная ошибка.")
# else:
#     print("Код выполнился без ошибок")

import pytest
import mysql.connector
from mysql.connector import errorcode


@pytest.fixture
def connect_db_two():
    try:
        cnx = mysql.connector.connect(host='host', user='user',
                                      password='password',
                                      database='database')

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        cursor = cnx.cursor()
        yield cursor
        cnx.close()